package jp.alhinc.takeshita_tomoki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店・商品定義ファイル読み込み
		if(!input(args[0], "branch.lst", branchNames, branchSales, "支店", "^\\d{3}$")) {
			return;
		}
		if(!input(args[0], "commodity.lst", commodityNames, commoditySales, "商品", "^[A-Za-z0-9]{8}$")) {
			return;
		}

		//売上ファイル読み込み、集計
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && (files[i].getName()).matches("^\\d{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt((rcdFiles.get(i).getName()).substring(0, 8));
			int latter = Integer.parseInt((rcdFiles.get(i + 1).getName()).substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				List<String> salesdata = new ArrayList<>();
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					salesdata.add(line);
				}
				if (salesdata.size() != 3) {
					System.out.println(file.getName() + "のフォーマットが不正です");
					return;
				}
				if (!branchSales.containsKey(salesdata.get(0))) {
					System.out.println(file.getName() + "の支店コードが不正です");
					return;
				}
				if (!commodityNames.containsKey(salesdata.get(1))) {
					System.out.println(file.getName() + "の商品コードが不正です");
					return;
				}
				if (!salesdata.get(2).matches("^\\d+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				Long saleAmount = branchSales.get(salesdata.get(0)) + Long.parseLong(salesdata.get(2));
				Long commoditySaleAmount = commoditySales.get(salesdata.get(1)) + Long.parseLong(salesdata.get(2));
				if (saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(salesdata.get(0), saleAmount);
				commoditySales.put(salesdata.get(1), commoditySaleAmount);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//ファイル出力
		if(!output(args[0], "branch.out", branchNames, branchSales)){
			return;
		}
		if(!output(args[0], "commodity.out", commodityNames, commoditySales)){
			return;
		}
	}

	//定義ファイル読み込みメソッド
	private static boolean input(String filepath, String inputName, Map<String, String> names, Map<String, Long> sales, String definitionFile, String regularExpression) {
		File file = new File(filepath, inputName);
		if (!file.exists()) {
			System.out.println(definitionFile +"定義ファイルが存在しません");
			return false;
		}
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if ((items.length != 2) || (!items[0].matches(regularExpression))) {
					System.out.println(definitionFile + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//ファイル出力メソッド
	private static boolean output(String filepath, String outputName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;

		try {
			File file = new File(filepath, outputName);
			bw = new BufferedWriter(new FileWriter(file));
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
